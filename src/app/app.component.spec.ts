import { async, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, ProductsComponent,
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });


  it('has products component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const productsElements: HTMLElement = fixture.nativeElement;
    const products = productsElements.querySelector('app-products');
    expect(products).toBeTruthy();
  });

});
