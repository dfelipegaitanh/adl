import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'creditCardMask'
})
export class CreditCardMaskPipe implements PipeTransform {

  transform(value: string, accountIdentifier: string): string {
    return accountIdentifier === 'CREDIT_CARD' ? 'xxxx xxxx xxxx xxxx' : value;
  }

}
