import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isLocked'
})
export class IsLockedPipe implements PipeTransform {

  transform(value: boolean ): string {
    return 'Is ' + (value) ? '' : 'NOT'  +  ' Locked';
  }

}
