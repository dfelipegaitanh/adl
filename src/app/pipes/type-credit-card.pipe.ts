import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeCreditCardPipe',
})
export class TypeCreditCardPipe implements PipeTransform{

  transform( creditCardNumber: string ): string {
    return ( /^5[1-5]/.test(creditCardNumber) )
      ? 'mastercard'
      : ( /^3[47]/.test(creditCardNumber) ? 'amex' : 'unknown' );
  }

}
