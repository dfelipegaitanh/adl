import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
      <app-products></app-products>`,
  // templateUrl: './app.component.html',
})
export class AppComponent implements OnInit{

  constructor() { }

  ngOnInit() {
  }
}
