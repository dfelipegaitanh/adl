import { Component, Inject } from '@angular/core';

import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IProduct } from '../../shared/interfaces';
import { productIcons } from 'src/app/shared/variablesNeeded';

@Component({
  selector: 'app-product-information',
  templateUrl: './product-information.component.html',
  styleUrls: [ './product-information.component.css' ],
})
// tslint:disable-next-line:component-class-suffix
export class ProductInformationDialog{

  productIcons: string[] = productIcons;

  constructor( @Inject(MAT_DIALOG_DATA) public data: IProduct ) { }
}
