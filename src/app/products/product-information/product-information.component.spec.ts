import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductInformationDialog } from './product-information.component';
describe('ProductInformationDialog', () => {
  let component: ProductInformationDialog;
  let fixture: ComponentFixture<ProductInformationDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductInformationDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductInformationDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

