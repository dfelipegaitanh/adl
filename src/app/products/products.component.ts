import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  // templateUrl: './products.component.html',
  template: `
      <app-products-list></app-products-list>`,
  styleUrls: [ './products.component.css' ],
})
export class ProductsComponent implements OnInit{

  ngOnInit(): void {
  }
}
