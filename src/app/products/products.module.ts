import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { ProductsComponent } from './products.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductInformationDialog } from './product-information/product-information.component';
import { CreditCardMaskPipe } from '../pipes/credit-card-mask.pipe';
import { TypeCreditCardPipe } from '../pipes/type-credit-card.pipe';
import { IsLockedPipe } from '../pipes/is-locked.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule(
  {
    imports: [
      CommonModule,
      MatListModule,
      MatCardModule,
      MatIconModule,
      MatButtonModule,
      MatDialogModule,
      FormsModule,
      ReactiveFormsModule,
    ],
    declarations: [
      ProductsComponent,
      ProductsListComponent,
      ProductInformationDialog,
      CreditCardMaskPipe,
      TypeCreditCardPipe,
      IsLockedPipe
    ],
    exports: [
      ProductsComponent,
    ],
  },
)
export class ProductsModule{}
