const productIcons = [];
productIcons[ 'CERTIFIED_DEPOSIT_TERM' ] = 'shopping_bag';
productIcons[ 'CREDIT' ] = 'card_giftcard';
productIcons[ 'CREDIT_CARD' ] = 'credit_card';
productIcons[ 'CURRENT_ACCOUNT' ] = 'account_balance_wallet';
productIcons[ 'DEPOSIT_ACCOUNT' ] = 'account_balance';

const banks = [ '', 'BANCO_1', 'BANCO_2', 'BANCO_3', 'BANCO_4' ];
const typeAccounts = [
  '',
  'CERTIFIED_DEPOSIT_TERM',
  'CREDIT',
  'CREDIT_CARD',
  'CURRENT_ACCOUNT',
  'DEPOSIT_ACCOUNT' ];

export { productIcons, banks, typeAccounts };
