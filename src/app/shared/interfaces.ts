export interface IAccountInformation{
  accountIdentifier: string;
  currencyCode?: string;
  productType: string;
  bank: string;
}

export interface ITermPeriodicityOfPayment{
  count: number;
  units: string;
}
export interface IAmountCurrencyCode{
  amount: number;
  rate?: number;
  currencyCode?: string;
}

export interface IProductAccountBalances{
  pago_total_pesos?: IAmountCurrencyCode;
  cupo_disponible_avances_pesos?: IAmountCurrencyCode;
  saldo_mora_pesos?: IAmountCurrencyCode;
  cupo_disponible_compras_pesos?: IAmountCurrencyCode;
  valor_pago_minimo?: IAmountCurrencyCode;
  cupo_total?: IAmountCurrencyCode;
  saldo_pendiente_pago?: IAmountCurrencyCode;
  cupo_aprobado_remesas?: IAmountCurrencyCode;
  cupos_aprobado_sobregiro?: IAmountCurrencyCode;
  cupo_disponible_sobregiro?: IAmountCurrencyCode;
  saldo_canje?: IAmountCurrencyCode;
  saldo_disponible?: IAmountCurrencyCode;
  saldo_canje_48_horas?: IAmountCurrencyCode;
  saldo_canje_72_horas?: IAmountCurrencyCode;
  saldo_canje_24_horas?: IAmountCurrencyCode;
  saldo_ayer?: IAmountCurrencyCode;
  saldo_actual?: IAmountCurrencyCode;
  tasa_nominal?: IAmountCurrencyCode;
  interes_pagado?: IAmountCurrencyCode;
  valor_constitucion?: IAmountCurrencyCode;
  intereses_causados?: IAmountCurrencyCode;
  retefuente?: IAmountCurrencyCode;
}

export interface ICreditCardDate{
  paymentDate: string;
}

export interface IProduct{
  accountInformation: IAccountInformation;
  locked: boolean;
  id: string;
  typeAccount: string;
  status?: string;
  openedDate?: string;
  closedDate?: string;
  dueDate?: string;
  creditCardsDates?: ICreditCardDate;
  lastTransactionDate?: string;
  overDraftDays?: number;
  term?: ITermPeriodicityOfPayment;
  periodicityOfPayment?: ITermPeriodicityOfPayment;
  productAccountBalances?: IProductAccountBalances;
  capacity?: number;
}
