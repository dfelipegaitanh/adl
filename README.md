# Adl Prueba Técnica


* Los productos se deben presentar en cards y agrupados por tipo de producto
  INFORMACION DE LAS CARDS
  * Tipo de producto
  * Icono de cada producto (puede ser fuente o imagen)
  * Saldo de la cuenta o del producto en cuestión
  * La card debe tener una versión maximizada con la información detallada del producto
  * En el caso de las tarjetas de crédito, se deben diferenciar si es visa o mastercard,
  * En el caso de las tarjetas de crédito, el número de la tarjeta debe estar enmascarado los primeros 12 caracteres,
  * el formato del número de las tarjetas de crédito es xxxx xxxx xxxx xxxx
  * En el caso de las tarjetas de crédito, si la tarjeta ya tiene fecha de pago, se debe presentar una opción visual que me lo indique
  * En el caso de las tarjetas de crédito, si la tarjeta no tiene fecha de pago, se debe indicar el total de la tarjeta y gasto actual en una barra de progreso
* Los productos iniciales que se deben presentar son los relacionado a BANCO_1
* Se debe crear una opción para mostrar y/o ocultar los productos de los otros bancos agrupados


# Como correr el proyecto

En la consola ejecutar `ng serve` y en el navegador ir a `http://localhost:4200/`.
